import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class GoogleHomePageTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
    }

    @Test
    public void urlTest() {
        new GoogleHomePage(driver);
        Assert.assertTrue(driver.getCurrentUrl().startsWith("https://www.google.com/"));
    }

    @Test
    public void simpleSearchTest() {
        new GoogleHomePage(driver).googleSearch("Selenium");
        String url = driver.getCurrentUrl();
        Assert.assertTrue(url.contains("www.google.com"));
        Assert.assertTrue(url.contains("Selenium"));
    }

    // Write a test method, that searches for Selenium and verify that the results page contains the link with text
    // Selenium - Web Browser Automation


    @AfterClass
    public void cleanUp() {
        try {
            driver.quit();
        } catch (Exception e) {/* Ignore error */}
    }
}
