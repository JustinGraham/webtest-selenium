import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

// A simple PageObject
public class GoogleHomePage {
    private final WebDriver driver;

    @FindBy(css = "input[name='q']")
    private WebElement searchField;

    @FindBy(css = "button[aria-label$='Search']")
    private WebElement searchButton;

    @FindBy(css = "button[aria-label$='Lucky']")
    private WebElement luckyButton;

    public GoogleHomePage(final WebDriver driver) {
        this.driver = driver;
        driver.get("http://www.google.com/");
        PageFactory.initElements(new AjaxElementLocatorFactory(driver, 5), this);
    }

    public GoogleResultPage googleSearch(final String query) {
        searchField.clear();
        searchField.sendKeys(query);
        searchButton.click();
        return new GoogleResultPage(driver);
    }

    public WebPage imFeelingLucky() {
        searchField.clear();
        luckyButton.click();
        return new WebPage(driver);
    }
}
