import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

// A simple PageObject
public class GoogleHomePage {

    public static final String URL = "http://www.google.com";

    public GoogleResultPage googleSearch(String query) {
        $(By.name("q")).setValue(query).pressEnter();
        return page(GoogleResultPage.class);
    }
}
