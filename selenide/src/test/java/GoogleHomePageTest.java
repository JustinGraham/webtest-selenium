import com.codeborne.selenide.ElementsCollection;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.codeborne.selenide.CollectionCondition.size;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;

@Test
public class GoogleHomePageTest {

    @Test
    public void urlTest() {
        open(GoogleHomePage.URL, GoogleHomePage.class);
        Assert.assertTrue(url().contains("https://www.google.com/"));
    }

    @Test
    public void simpleSearchTest() {
        final ElementsCollection results = open(GoogleHomePage.URL, GoogleHomePage.class)
                .googleSearch("selenide")
                .results();

        results.shouldHave(size(10));
        results.get(0).shouldHave(text("Selenide: Concise UI Tests in Java"));
    }
}
